import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { RequestMethod, ValidationPipe } from '@nestjs/common';
import * as morgan from 'morgan';
import * as chalk from 'chalk';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // http logs
  if (process.env.NODE_ENV !== 'production')
    app.use(
      morgan(
        chalk`:date[iso] {red :method} :url {green :status} {blue :response-time ms} - :res[content-length] length`,
      ),
    );
  else
    app.use(
      morgan(
        ':date[iso] :method :url :status :response-time ms - :res[content-length] length',
      ),
    );

  // http logs

  app.setGlobalPrefix('api', {
    exclude: [{ path: '/', method: RequestMethod.GET }],
  });

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      validatorPackage: require('@nestjs/class-validator'),
      transformerPackage: require('@nestjs/class-transformer'),
    }),
  );

  const config = new DocumentBuilder()
    .setTitle('Lite Docs API')
    .setDescription('API for Lite backend')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  app.enableCors();

  await app.listen(process.env.PORT || 4000);
}
bootstrap();
