/* eslint-disable @typescript-eslint/no-var-requires */
import { Injectable } from '@nestjs/common';
// import { ConfigType } from '@nestjs/config';
// import { config } from 'src/config';
// Load the AWS SDK for Node.js
const AWS = require('aws-sdk');
// Set the region
AWS.config.update({ region: 'us-east-1' });

const nodemailer = require('nodemailer');

const SENDER_MAIL = 'no-reply@copywritecol.com';

@Injectable()
export class EmailService {
  private ses;
  private nodeMailerTransport;

  constructor() {
    const ses = new AWS.SES({ apiVersion: '2010-12-01' });
    this.ses = ses;
    this.nodeMailerTransport = nodemailer.createTransport({
      SES: { ses, aws: AWS },
    });
  }

  sendMailWithFiles(
    to: string,
    subject: string,
    htmlMessage: string,
    attachments: any[],
  ) {
    // send some mail
    this.nodeMailerTransport.sendMail(
      {
        from: SENDER_MAIL,
        to,
        subject,
        // text: 'I hope this message gets sent!',
        html: htmlMessage,
        /*ses: {
          // optional extra arguments for SendRawEmail
          Tags: [
            {
              Name: 'tag_name',
              Value: 'tag_value',
            },
          ],
        },*/
        attachments,
      },
      (err, info) => {
        if (err) {
          console.error(err);
        }
        if (info) {
          console.log(info.envelope);
          console.log(info.messageId);
        }
      },
    );
  }

  sendMail(
    to: string,
    subject: string,
    htmlMessage: string,
    cc?: string,
    // configModule: ConfigType<typeof config>,
  ) {
    // Create sendEmail params
    const params = {
      Destination: {
        /* required */
        CcAddresses: cc ? cc : [],
        ToAddresses: [
          to,
          /* more items */
        ],
      },
      Message: {
        /* required */
        Body: {
          /* required */
          Html: {
            Charset: 'UTF-8',
            Data: htmlMessage,
          },
          Text: {
            Charset: 'UTF-8',
            Data: htmlMessage,
          },
        },
        Subject: {
          Charset: 'UTF-8',
          Data: subject,
        },
      },
      Source: SENDER_MAIL /* required */,
      ReplyToAddresses: [
        //'EMAIL_ADDRESS',
        /* more items */
      ],
    };

    // Create the promise and SES service object
    const sendPromise = this.ses.sendEmail(params).promise();

    // Handle promise's fulfilled/rejected states
    sendPromise
      .then(function (data) {
        console.log(`EMAIL SENT(${to}): ${data.MessageId}`);
      })
      .catch(function (err) {
        console.error(err, err.stack);
      });
  }

  getCCEnvAddress() {
    // configModule
    if (process.env.EMAILS_CC) {
      try {
        return process.env.EMAILS_CC.split(',');
      } catch (error) {
        console.log(error.message);
      }
    }
    return [];
  }
}
