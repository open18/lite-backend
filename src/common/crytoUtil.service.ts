import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

const saltRounds = 10;

@Injectable()
export class CrytoUtilService {
  hashPassword(text) {
    return bcrypt.hashSync(text, saltRounds);
  }
  comparePassword(text, hash) {
    if (text && hash) return bcrypt.compareSync(text, hash);
    return false;
  }
}
