/* eslint-disable @typescript-eslint/no-var-requires */
import { Injectable, Logger } from '@nestjs/common';

const {
  S3Client,
  PutObjectCommand,
  GetObjectCommand,
} = require('@aws-sdk/client-s3');
const mime = require('mime');
const fs = require('fs');
const os = require('os');

const BUCKET_NAME = 'lite-files';
const REGION = 'us-east-1';

@Injectable()
export class FilesService {
  private readonly logger = new Logger(FilesService.name);
  private s3Client;

  constructor() {
    this.s3Client = new S3Client({ region: REGION });
  }

  async uploadFileFromPath(path: string, bucketPath: string) {
    this.logger.log('uploadFileFromPath: ', path, bucketPath);
    const fileType = mime.getType(path);

    const params = {
      Bucket: BUCKET_NAME,
      ContentType: fileType,
      Key: bucketPath,
      Body: fs.readFileSync(path),
      ACL: 'public-read',
    };

    // Create an object and upload it to the Amazon S3 bucket.
    try {
      await this.s3Client.send(new PutObjectCommand(params));
      const url = `https://${BUCKET_NAME}.s3.${REGION}.amazonaws.com/${bucketPath}`;
      return url;
    } catch (err) {
      // console.log(err.stack)
      this.logger.error('Error', err);
    }
    return null;
  }

  async readFileFromPath(bucketPath) {
    const params = {
      Bucket: BUCKET_NAME,
      ContentType: 'application/json',
      Key: bucketPath,
      // Body: fs.readFileSync(path),
      ACL: 'public-read',
    };

    // Create an object and upload it to the Amazon S3 bucket.
    try {
      const response = await this.s3Client.send(new GetObjectCommand(params));

      return new Promise((resolve) => {
        // Store all of data chunks returned from the response data stream
        // into an array then use Array#join() to use the returned contents as a String
        const responseDataChunks = [];

        // Attach a 'data' listener to add the chunks of data to our array
        // Each chunk is a Buffer instance
        response.Body.on('data', (chunk) => responseDataChunks.push(chunk));

        // Once the stream has no more data, join the chunks into a string and return the string
        response.Body.once('end', () => resolve(responseDataChunks.join('')));
      });
    } catch (err) {
      this.logger.error('Error', err);
    }
    return null;
  }

  async saveBufferFileInTempFolder(file: Express.Multer.File, id: string) {
    let fileExtension = '';
    if (file.mimetype === 'application/octet-stream')
      fileExtension = this.getExtensionWithFileName(file.originalname);
    else fileExtension = mime.getExtension(file.mimetype);

    const nameFile = `${id}.${fileExtension}`;
    const rootFolder = `${os.tmpdir()}/lite-files`;
    const tempPath = `${rootFolder}/${nameFile}`;

    await fs.mkdirSync(rootFolder, { recursive: true });
    await fs.writeFileSync(tempPath, file.buffer);
    return { tempPath, nameFile };
  }

  getExtensionWithFileName(filename) {
    return filename.split('.').pop();
  }
}
