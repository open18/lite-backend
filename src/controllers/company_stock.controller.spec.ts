import { Test, TestingModule } from '@nestjs/testing';
import { CompanyStockController } from './company_stock.controller';

describe('CompanyStockController', () => {
  let controller: CompanyStockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CompanyStockController],
    }).compile();

    controller = module.get<CompanyStockController>(CompanyStockController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
