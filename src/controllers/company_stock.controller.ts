import {
  Body,
  Controller,
  Delete,
  Get,
  MaxFileSizeValidator,
  Param,
  ParseFilePipe,
  Post,
  Put,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Roles } from 'src/auth/decorators/roles.decoratos';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Role } from 'src/auth/models/roles.model';
import { CompanyStockService } from 'src/services/company_stock.service';

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('company-stock')
export class CompanyStockController {
  constructor(private companyStockService: CompanyStockService) {}

  @Roles(Role.ADMIN)
  @Post()
  create(@Body() body: any) {
    return this.companyStockService.create(body);
  }

  @Roles(Role.ADMIN)
  @Put()
  update(@Body() body: any) {
    return this.companyStockService.update(body);
  }

  @Roles(Role.ADMIN)
  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.companyStockService.delete(id);
  }

  @Roles(Role.ADMIN, Role.EXTERNAL)
  @Get('/idCompany/:idCompany')
  findAllByIdCompany(@Param('idCompany') idCompany: string) {
    return this.companyStockService.findAllByIdCompany(idCompany);
  }

  @Roles(Role.ADMIN, Role.EXTERNAL)
  @Post('send-email')
  @UseInterceptors(FileInterceptor('file'))
  sendEmail(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 1048576 * 40 }), // 40MB
        ],
      }),
    )
    file: Express.Multer.File,
    @Body() data: any,
  ) {
    return this.companyStockService.sendEmail(file, data);
  }
}
