import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Roles } from 'src/auth/decorators/roles.decoratos';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Role } from 'src/auth/models/roles.model';
import { UserSession } from 'src/auth/models/user-session.model';
import { CompanyService } from 'src/services/company.service';
import { Request } from 'express';

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('company')
export class CompanyController {
  constructor(private companyService: CompanyService) {}

  @Roles(Role.ADMIN)
  @Post()
  create(@Req() req: Request, @Body() body: any) {
    const user = req.user as UserSession;
    return this.companyService.create(body, user);
  }

  @Roles(Role.ADMIN, Role.EXTERNAL)
  @Get()
  findAll() {
    return this.companyService.findAll();
  }

  @Roles(Role.ADMIN)
  @Put()
  update(@Body() body: any) {
    return this.companyService.update(body);
  }

  @Roles(Role.ADMIN)
  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.companyService.delete(id);
  }
}
