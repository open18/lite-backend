import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserSession } from 'src/auth/models/user-session.model';
import { Company } from 'src/entities/company.entity';

@Injectable()
export class CompanyService {
  constructor(
    @InjectModel(Company.name) private companyModel: Model<Company>,
  ) {}

  async create(data: any, user: UserSession) {
    const { nit, name, address, phone } = data;
    const existsCompany = await this.companyModel.findOne({ nit });
    if (existsCompany) throw new BadRequestException('COMPANY_EXISTS');

    const newCompany = new this.companyModel({
      nit,
      name,
      address,
      phone,
      id_user_creator: user.sub,
    });
    const newSaved = await newCompany.save();

    return { message: 'OK', data: newSaved };
  }

  async findAll() {
    const data = await this.companyModel.find().sort({ created_at: -1 });
    return { message: 'OK', data };
  }

  async update(data: any) {
    const { name, address, phone, idCompany } = data;
    const company = await this.companyModel.findById(idCompany);
    if (!company) throw new BadRequestException('COMPANY_NOT_EXISTS');

    company.name = name;
    company.address = address;
    company.phone = phone;
    const companySaved = await company.save();

    return { message: 'OK', data: companySaved };
  }

  async delete(id: string) {
    await this.companyModel.findByIdAndDelete(id);
    return { message: 'OK' };
  }
}
