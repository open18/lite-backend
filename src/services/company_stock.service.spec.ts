import { Test, TestingModule } from '@nestjs/testing';
import { CompanyStockService } from './company_stock.service';

describe('CompanyStockService', () => {
  let service: CompanyStockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CompanyStockService],
    }).compile();

    service = module.get<CompanyStockService>(CompanyStockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
