import { BadRequestException, Injectable } from '@nestjs/common';
import { CompanyStock } from 'src/entities/company_stock.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Company } from 'src/entities/company.entity';
import { EmailService } from 'src/common/email.service';

@Injectable()
export class CompanyStockService {
  constructor(
    @InjectModel(Company.name) private companyModel: Model<Company>,
    @InjectModel(CompanyStock.name)
    private companyStockModel: Model<CompanyStock>,
    private emailService: EmailService,
  ) {}

  async create(data: any) {
    const { name, amount, idCompany } = data;
    const existsCompany = await this.companyModel.findById(idCompany);
    if (!existsCompany) throw new BadRequestException('COMPANY_NOT_EXISTS');

    const newCompanyStock = new this.companyStockModel({
      name,
      amount,
      id_company: existsCompany.id,
    });
    const newCompanyStockSaved = await newCompanyStock.save();

    return { message: 'OK', data: newCompanyStockSaved };
  }

  async update(data: any) {
    const { name, amount, idCompanyStock } = data;
    const companyStock = await this.companyStockModel.findById(idCompanyStock);
    if (!companyStock)
      throw new BadRequestException('COMPANY_STOCK_NOT_EXISTS');

    companyStock.name = name;
    companyStock.amount = amount;
    const newCompanyStockSaved = await companyStock.save();

    return { message: 'OK', data: newCompanyStockSaved };
  }

  async delete(id: string) {
    await this.companyStockModel.findByIdAndDelete(id);
    return { message: 'OK' };
  }

  async findAllByIdCompany(idCompany: string) {
    const data = await this.companyStockModel
      .find({ id_company: idCompany })
      .sort({ created_at: -1 });
    return { message: 'OK', data };
  }

  async sendEmail(file: Express.Multer.File, body: any) {
    const { emailTo } = body;

    if (file && emailTo) {
      const attachments = [
        {
          filename: file.originalname,
          content: file.buffer,
        },
      ];

      const subject = 'Test envio correo!';
      const htmlMessage = `<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width,initial-scale=1"><title>¡Test!</title></head><body><span>Hola.<br><br><br><br><span><b>Test envio correo</b></span><br><br></body></html>`;

      await this.emailService.sendMailWithFiles(
        emailTo,
        subject,
        htmlMessage,
        attachments,
      );
      return { message: 'OK' };
    }

    throw new BadRequestException('BAD_PARAMS');
  }
}
