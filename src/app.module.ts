import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CompanyController } from './controllers/company.controller';
import { CompanyService } from './services/company.service';
import { CompanyStockController } from './controllers/company_stock.controller';
import { CompanyStockService } from './services/company_stock.service';
import { AuthModule } from './auth/auth.module';
import { configModule } from './config';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './database/database.module';
import { databaseAllModules } from './database/database.all.modules';
import { FilesService } from './common/files.service';
import { EmailService } from './common/email.service';

@Module({
  imports: [
    ...databaseAllModules,
    ConfigModule.forRoot(configModule),
    DatabaseModule,
    AuthModule,
  ],
  controllers: [AppController, CompanyController, CompanyStockController],
  providers: [
    AppService,
    CompanyService,
    CompanyStockService,
    EmailService,
    FilesService,
  ],
})
export class AppModule {}
