import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { PayloadToken } from 'src/auth/models/token.model';
import { JwtService } from '@nestjs/jwt';
import { CrytoUtilService } from 'src/common/crytoUtil.service';
import { UserSession } from 'src/auth/models/user-session.model';
import { Role } from 'src/auth/models/roles.model';
import { config } from 'src/config';
import { ConfigType } from '@nestjs/config';
import { User } from 'src/entities/user.entity';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private crytoUtilService: CrytoUtilService,
    @Inject(config.KEY) private configModule: ConfigType<typeof config>,
    @InjectModel(User.name) private userModel: Model<User>,
  ) {}

  async validateUser(username: string, password: string) {
    const user = await this.userModel.findOne({ email: username });
    if (user) {
      const isMatch = this.crytoUtilService.comparePassword(
        password,
        user.password,
      );
      if (isMatch) {
        return new UserSession(user.role, user._id, user.email);
      }
    }
    return null;
  }

  generateJWT(user: UserSession) {
    const payload: PayloadToken = {
      role: user.role,
      sub: user.id,
    };
    return {
      access_token: this.jwtService.sign(payload),
      user,
    };
  }

  async createUser(data: any) {
    const { email, password, role } = data;

    if (!Object.values(Role).includes(role))
      throw new BadRequestException('BAD_ROLE');

    const existsUser = await this.userModel.findOne({ email });
    if (existsUser) throw new BadRequestException('USER_EXISTS');

    const newUser = new this.userModel({
      email,
      password: this.crytoUtilService.hashPassword(password),
      role: role === 'ADMIN' ? Role.ADMIN : Role.EXTERNAL,
    });

    const userSaved = await newUser.save();

    return { message: 'OK', data: userSaved };
  }
}
