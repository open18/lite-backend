import { Body, Controller, Param, Post, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../services/auth/auth.service';
import { UserSession } from '../models/user-session.model';
import { Public } from '../decorators/public.decorator';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  login(@Req() req: Request) {
    const user = req.user as UserSession;
    return this.authService.generateJWT(user);
  }

  @Public()
  @Post('create-user')
  createUser(@Body() data: any) {
    return this.authService.createUser(data);
  }
}
