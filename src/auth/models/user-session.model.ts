import { Role } from './roles.model';

export class UserSession {
  role: Role;
  id: string;
  email: string;
  sub: string;

  constructor(role: Role, id: string, email: string) {
    this.role = role;
    this.id = id;
    this.sub = id;
    this.email = email;
  }
}
