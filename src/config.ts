import { registerAs } from '@nestjs/config';
import { enviroments } from './environments';
import * as Joi from 'joi';

export const config = registerAs('config', () => {
  return {
    apiKey: process.env.API_KEY,
    jwtSecret: process.env.JWT_SECRET,
    mongo: {
      uri: process.env.MONGO_URI,
    },
  };
});

export const configModule = {
  envFilePath: enviroments[process.env.NODE_ENV] || '.env',
  load: [config],
  isGlobal: true,
  validationSchema: Joi.object({
    API_KEY: Joi.number().required(),
    JWT_SECRET: Joi.string().not().empty().required(),
    MONGO_URI: Joi.string().not().empty().required(),
  }),
};
