import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

@Schema({
  versionKey: false,
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
})
export class Company extends Document {
  @Prop({ unique: true })
  nit: string;

  @Prop()
  name: string;

  @Prop()
  address: string;

  @Prop()
  phone: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId })
  id_user_creator: mongoose.Schema.Types.ObjectId;
}

export const CompanySchema = SchemaFactory.createForClass(Company);
