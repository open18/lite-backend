import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

@Schema({
  versionKey: false,
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  collection: 'company_stock',
})
export class CompanyStock extends Document {
  @Prop()
  name: string;

  @Prop()
  amount: number;

  @Prop({ type: mongoose.Schema.Types.ObjectId })
  id_company: mongoose.Schema.Types.ObjectId;
}

export const CompanyStockSchema = SchemaFactory.createForClass(CompanyStock);
