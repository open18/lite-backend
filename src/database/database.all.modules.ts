import { MongooseModule } from '@nestjs/mongoose';
import { Company, CompanySchema } from 'src/entities/company.entity';
import {
  CompanyStock,
  CompanyStockSchema,
} from 'src/entities/company_stock.entity';
import { User, UserSchema } from 'src/entities/user.entity';

export const databaseAllModules = [
  MongooseModule.forFeature([
    {
      name: User.name,
      schema: UserSchema,
    },
  ]),
  MongooseModule.forFeature([
    {
      name: Company.name,
      schema: CompanySchema,
    },
  ]),
  MongooseModule.forFeature([
    {
      name: CompanyStock.name,
      schema: CompanyStockSchema,
    },
  ]),
];
